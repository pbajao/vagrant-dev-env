# Vagrant Dev Env

Development environment setup using Vagrant.

## Requirements

- [Vagrant](https://www.vagrantup.com/)
- [VirtualBox](https://www.virtualbox.org/)
- SSH public key generated as `$HOME/.ssh/id_ed25519.pub`
- SSH private key generated as `$HOME/.ssh/id_ed25519`

## Setup

1. Clone this repository in a directory.
1. Install `vagrant-vbguest` Vagrant plugin: `vagrant plugin install vagrant-vbguest`
1. Run `vagrant up`.
1. Wait for it to finish.
1. Once finished, you can now access the machine via `vagrant ssh`.

## Tools Installed

- [Docker](https://docs.docker.com/engine/) with [Compose](https://docs.docker.com/compose/)
- [RVM](https://rvm.io/) for Ruby version management
- [NVM](https://github.com/nvm-sh/nvm) for Node version management

## Network

Vagrant machine is set up with a bridge so it can be accessible
within your network.

### Getting IP addresses of Vagrant machine

There is a convenience script added called `ips.sh`. Running it
will call `ifconfig` on the machine and prints out the output.

On Windows PowerShell, there's a `ips.ps1` script available.

This is useful if you need to access the machine outside Vagrant
(e.g. accessing port 3000 via http).
