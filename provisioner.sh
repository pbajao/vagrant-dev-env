#!/bin/bash

echo "Setting up requirements..."
sudo apt-get update
sudo apt-get -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common net-tools
echo "Done!"

echo "Setting up Docker repository..."
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
echo "Done!"

echo "Installing Docker..."
sudo apt-get update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io docker-compose
sudo usermod -aG docker vagrant
echo "Done!"

echo "Importing SSH key from host..."
mkdir -p /home/vagrant/.ssh/
echo $SSH_PUB_KEY > /home/vagrant/.ssh/id_ed25519.pub
chmod 644 /home/vagrant/.ssh/id_ed25519.pub
echo -e "$SSH_PRV_KEY" > /home/vagrant/.ssh/id_ed25519
chmod 600 /home/vagrant/.ssh/id_ed25519
chown -R vagrant:vagrant /home/vagrant
echo "Done!"

echo "Setting up dotfiles..."
if [ ! -d /home/vagrant/dotfiles ]
then
  ssh-keyscan -t rsa gitlab.com >> /home/vagrant/.ssh/known_hosts
  git clone git@gitlab.com:pbajao/dotfiles.git /home/vagrant/dotfiles
  cd /home/vagrant/dotfiles && ./link.sh
fi
cd /home/vagrant/dotfiles && git pull origin master
echo "Done!"

echo "Installing RVM..."
sudo apt-add-repository -y ppa:rael-gc/rvm
sudo apt-get update
sudo apt-get -y install rvm
sudo usermod -a -G rvm vagrant
echo "Done!"

echo "Installing NVM..."
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.37.2/install.sh | bash
echo "Done!"